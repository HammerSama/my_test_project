package OpenJPA;

//import OpenJPA.Producer;
import org.apache.openjpa.persistence.OpenJPAPersistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OpenJPA {

    public static void main(String[] args) {
        //Producer p = new Producer("FPT SS17 Producer");
        Pizza p = new Pizza(5,"Spinacci",6.5f,5f);

        EntityManagerFactory fac = getWithoutConfig();

        //EntityManager erstellen
        EntityManager e = fac.createEntityManager();

        //Entity Transaction bekommen
        EntityTransaction t = e.getTransaction();
        //Transaktion durchführen, p in Datenbank speichern
        t.begin();
        e.persist(p);
        t.commit();

        t.begin();
        for (Object o : e.createQuery("SELECT x FROM Pizza x")
                .getResultList()) {
            Pizza z = (Pizza) o;
            System.out.println(z.getId()+") "+z.getName() + ", Small: " +z.getSmall()+", Big: "+z.getBig());
        }
        t.commit(); // all ok commit
        //Daten jetzt in der Datenbank gespeichert

        //Verbindungen schließen
        e.close();
        fac.close();

    }



    public static EntityManagerFactory getWithoutConfig() {

        Map<String, String> map = new HashMap<String, String>();

        map.put("openjpa.ConnectionURL","jdbc:sqlite:pizza.db");
        map.put("openjpa.ConnectionDriverName", "org.sqlite.JDBC");
        map.put("openjpa.RuntimeUnenhancedClasses", "supported");
        map.put("openjpa.jdbc.SynchronizeMappings", "false");

        // find all classes to registrate them
        List<Class<?>> types = new ArrayList<Class<?>>();
        types.add(Pizza.class);

        if (!types.isEmpty()) {
            StringBuffer buf = new StringBuffer();
            for (Class<?> c : types) {
                if (buf.length() > 0)
                    buf.append(";");
                buf.append(c.getName());
            }
            // <class>Pizza</class>
            map.put("openjpa.MetaDataFactory", "jpa(Types=" + buf.toString()+ ")");
        }

        return OpenJPAPersistence.getEntityManagerFactory(map);

    }
}
