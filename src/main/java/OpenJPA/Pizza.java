package OpenJPA;

import javax.persistence.*;
import java.io.Serializable;


@Entity()
@Table(name = "Pizza")
public class Pizza implements Serializable {

    private long id;
    private String name;
    private float big;
    private float small;

    public Pizza() {

    }

    public Pizza(String name, float big, float small) {
        this.setName(name);
        this.setBig(big);
        this.setSmall(small);
    }

    public Pizza(long id,String name, float big, float small) {
        this.setId(id);
        this.setName(name);
        this.setBig(big);
        this.setSmall(small);
    }

    @Id
    @Column(name="id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="big")
    public float getBig() {
        return big;
    }

    public void setBig(float big) {
        this.big = big;
    }

    @Column(name="small")
    public float getSmall() {
        return small;
    }

    public void setSmall(float small) {
        this.small = small;
    }

}
