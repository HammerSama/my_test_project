package controller;

import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import main.Playlist;
import main.Song;
import model.Model;
import view.View;

import java.io.File;
import java.rmi.RemoteException;

/**
 * Created by haoha on 2017/11/3.
 */
public class Controller {
    private Model model;
    private View view;
    public void link(Model model,View view)throws RemoteException {
        view.getAllSongs().setItems((ObservableList<interfaces.Song>) model.getAlleLieder());
        view.setListViewShow(view.getAllSongs());
        view.addEventAddAll(e->{
            try {
                model.addAllToPlaying();
                view.getPlaying().setItems((ObservableList<interfaces.Song>) model.getPlayList() );
                view.setListViewShow(view.getPlaying());
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }

        });

        view.addEventAddToPlayList(e->{
            try {
                model.addNewToPlaying(view.getSelected());
                view.getPlaying().setItems((ObservableList<interfaces.Song>) model.getPlayList() );
                view.setListViewShow(view.getPlaying());
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });

        view.addEventClearAll(e->{
            try {
                model.clearPlaying();
                view.getPlaying().setItems((ObservableList<interfaces.Song>) model.getPlayList() );
                view.setListViewShow(view.getPlaying());
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });

        view.addEventCommit(e->{
            view.getAllSongs().getSelectionModel().getSelectedItem().setInterpret(view.getInterpretT());
            view.getAllSongs().getSelectionModel().getSelectedItem().setAlbum(view.getAlbumT());
        });

        view.addEventLoad(e->{
            if(view.getPath()!=""){
                try {
                    model.setMusicPath(view.getPath());
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                try {
                    view.getAllSongs().setItems((ObservableList<interfaces.Song>) model.getAlleLieder());
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
                view.setListViewShow(view.getAllSongs());
            }

        });



    }

}
