package SQLite;

import java.sql.*;

public class SQlite {

    public static void main(String[] args) {

        // Loading driver (can be skipped since java 1.5)
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection con = DriverManager.getConnection("jdbc:sqlite:pizza.db")) {
            executeMyStatement(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void executeMyStatement(Connection con) {

        create(con);
        delete(con,1);
        insert(con, 1, "Margarita", 3.5f, 4.5f);
        insert(con, 2, "Salami", 4.5f, 5.5f);
        insert(con, 3, "Schinken", 4.5f, 6f);
        insert(con,4, "Pilze",4f,5.5f);
        select(con);
    }

    private static void create(Connection con) {
        try (PreparedStatement pstmt = con.prepareStatement("CREATE TABLE IF NOT EXISTS Pizza (id integer, name text, small float, big float);")) {
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static boolean insert(Connection con, int id, String name, float small, float big) {
        try (PreparedStatement pstmt = con.prepareStatement(
                "INSERT INTO Pizza (id, name, small, big) VALUES (?,?,?,?);")) {
            pstmt.setInt(1,id);
            pstmt.setString(2,name);
            pstmt.setFloat(3,small);
            pstmt.setFloat(4,big);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void delete(Connection con, int id) {
        try (PreparedStatement pstmt = con.prepareStatement(
                "DELETE FROM Pizza WHERE id = ?;")) {
            pstmt.setInt(1,id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void select(Connection con) {
        try (PreparedStatement pstmt = con.prepareStatement("SELECT * FROM Pizza;");
             ResultSet rs = pstmt.executeQuery()) {
            while (rs.next()) {
                System.out.println(rs.getInt("id") + ") " + rs.getString("name") + ", Small: " + rs.getFloat("small")+ ", big: " + rs.getFloat("big"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
