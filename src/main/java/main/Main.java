package main;

import controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Model;
import view.View;

import java.rmi.RemoteException;

/**
 * Created by haoha on 2017/11/3.
 */
public class Main extends Application {
    public Main() throws RemoteException {
    }
    public static void main(String[] args){
        Application.launch(args);
    }
    View view = new View();
    Model model = new Model();
    Controller c = new Controller();
    @Override
    public void start(Stage primaryStage) throws Exception {
        c.link(model,view);
        primaryStage.setTitle("Music Player");
        primaryStage.setScene(view.getPlayingScene());
        primaryStage.show();
    }
}
