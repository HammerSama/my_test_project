package main;

import interfaces.*;
import interfaces.Song;
import javafx.collections.FXCollections;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by asd28 on 2017/10/26.
 */
public class Playlist extends ModifiableObservableListBase<Song> implements interfaces.Playlist {
    ArrayList<Song> Songs = new ArrayList<>();
    public Playlist(){
    }
    @Override
    public boolean addSong(Song s) throws RemoteException {
        super.add(s);
        return true;
        //return Songs.add(s);
    }

    @Override
    public boolean deleteSong(Song s) throws RemoteException {
        super.remove(s);
        return true;
        //return Songs.remove(s);
    }

    @Override
    public boolean deleteSongByID(long id) throws RemoteException {
        for(int i =1;i<Songs.size();i++){
            if(Songs.get(i).getId() ==id){
                return Songs.remove(Songs.get(i));
            }
        }
        return false;
    }

    @Override
    public void setList(ArrayList<Song> s) throws RemoteException {
        for(int i=0;i<s.size();i++){
            super.add(s.get(i));
        }

    }

    @Override
    public ArrayList<Song> getList() throws RemoteException {
        return this.Songs;
    }

    @Override
    public void clearPlaylist() throws RemoteException {
        super.clear();

    }

    @Override
    public int sizeOfPlaylist() throws RemoteException {
        return this.Songs.size();
    }

    @Override
    public Song findSongByPath(String name) throws RemoteException {
        for(int i =1;i<Songs.size();i++){
            if(Songs.get(i).getPath().equals(name)){
                return Songs.get(i);
            }
        }
        return null;
    }

    @Override
    public Song findSongByID(long id) throws RemoteException {
        for(int i =1;i<Songs.size();i++){
            if(Songs.get(i).getId() == id){
                return Songs.get(i);
            }
        }
        return null;
    }

    @Override
    public Iterator<Song> iterator() {
        return this.Songs.iterator();
    }
//Methoden von ModifiableObservableListBase
    @Override
    public Song get(int index) {
        return this.Songs.get(index);
    }

    @Override
    public int size() {
        return this.Songs.size();
    }

    @Override
    public void doAdd(int index, Song element) {
        this.Songs.add(index,element);

    }

    @Override
    public Song doSet(int index, Song element) {
        return this.Songs.set(index,element);
    }

    @Override
    public Song doRemove(int index) {
        return this.Songs.remove(index);
    }
}
