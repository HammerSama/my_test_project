package main.SerBinaer;

public class IDGenerator {
    private static long currentId = 0;
    private static final long MAXID = 9999;

    public static long getNextId() throws IDOverflowException {
        if (hasNext()) {
            return currentId++;
        }
        throw new IDOverflowException();
    }

    private static boolean hasNext() {
        return currentId < MAXID;
    }
}

class IDOverflowException extends Exception {
    public IDOverflowException() {
        super("Could not get next ID. Maximum number of IDs already assigned.");
    }
}
