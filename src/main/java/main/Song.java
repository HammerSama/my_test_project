package main;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

/**
 * Created by asd28 on 2017/10/26.
 */
public class Song implements interfaces.Song {
    SimpleIntegerProperty ID = new SimpleIntegerProperty();
    SimpleStringProperty Pfad = new SimpleStringProperty();
    SimpleStringProperty Titel = new SimpleStringProperty();
    SimpleStringProperty Album = new SimpleStringProperty();
    SimpleStringProperty Interpreten = new SimpleStringProperty();
    public Song(){

    }

    public Song(String string){
        this.setTitle(string);
    }

    public String getAlbum() {
        return this.Album.getValue();
    }

    @Override
    public void setAlbum(String album) {
        this.Album.set(album);

    }

    @Override
    public String getInterpret() {
        return this.Interpreten.getValue();
    }

    @Override
    public void setInterpret(String interpret) {
        this.Interpreten.setValue(interpret);

    }

    @Override
    public String getPath() {
        return this.Pfad.getValue();
    }

    @Override
    public void setPath(String path) {
        this.Pfad.setValue(path);

    }

    @Override
    public String getTitle() {
        return this.Titel.getValue();
    }

    @Override
    public void setTitle(String title) {
        this.Titel.setValue(title);

    }

    @Override
    public long getId() {
        return this.ID.getValue();
    }

    @Override
    public void setId(long id) {
        this.ID.setValue(id);

    }

    @Override
    public ObservableValue<String> pathProperty() {
        return this.Pfad;
    }

    @Override
    public ObservableValue<String> albumProperty() {
        return this.Album;
    }

    @Override
    public ObservableValue<String> interpretProperty() {
        return this.Interpreten;
    }

    @Override
    public ObservableValue<String> titleProperty() {
        return this.Titel;
    }
}
