package main.tcp;

import java.io.*;
import java.net.*;

public class GGTClient {

	public static void main(String[] args) {

		// Verbindungsanfrage/Verbindungsaufbau
		// Streams anlegen
		try (Socket serverCon = new Socket("localhost", 3141);
			 InputStream in = serverCon.getInputStream();
			 ObjectInputStream ois = new ObjectInputStream(in);
			 OutputStream out = serverCon.getOutputStream();
			 ObjectOutputStream oos = new ObjectOutputStream(out)) {
				oos.writeObject("hello");
				oos.flush();

				String s=(String) ois.readObject();
				System.out.println(s);




		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}