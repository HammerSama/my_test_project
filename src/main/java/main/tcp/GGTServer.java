package main.tcp;

import java.io.*;
import java.net.*;
public class GGTServer {
	public static void main(String[] args) {
		// ServerSocket erstellen
		try (ServerSocket server = new ServerSocket(3141)) {
			// Timeout nach 1 Minute
			// server.setSoTimeout(60000);
			while (true) {
				try (Socket client = server.accept();
                     InputStream in = client.getInputStream();
                     ObjectInputStream ois = new ObjectInputStream(in);
                     OutputStream out = client.getOutputStream();
                     ObjectOutputStream oos = new ObjectOutputStream(out)) {
					// Streams erstellen
					// Zahlen einlesen
                    String s = (String) ois.readObject();
                    oos.writeObject(s);
                    oos.flush();
					// Ergebnis auf Outputstream schreiben
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	public static int ggT(int a, int b) {

		if (b == 0) {
			return a;
		} else {
			return ggT(b, a % b);
		}
	}
}