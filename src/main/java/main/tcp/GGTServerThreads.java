package TCP;

import java.io.*;
import java.net.*;

public class GGTServerThreads {

	public static void main(String[] args) {

		// ServerSocket erstellen
		try (ServerSocket server = new ServerSocket(3141);) {
			int connections = 0;
			// Timeout nach 1 Minute
			// server.setSoTimeout(60000);
			while (true) {
				try {
					Socket socket = server.accept();
					connections++;
					new MyTCPThread(connections, socket).start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}

class MyTCPThread extends Thread {
	private int name;
	private Socket socket;

	public MyTCPThread(int name, Socket socket) {
		this.name = name;
		this.socket = socket;
	}

	public void run() {
		String msg = "EchoServer: Verbindung " + name;
		System.out.println(msg + " hergestellt");
		try (InputStream in = socket.getInputStream();
				OutputStream out = socket.getOutputStream()) {
			try {
				sleep((long) (Math.random() * 10000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int a = in.read();
			int b = in.read();

			int result = ggT(a, b);

			// Ergebnis auf Outputstream schreiben
			out.write(result);
			out.flush();

			System.out.println("GGT von "+a+" und "+b+" ist "+result);
			System.out.println("Verbindung " + name + " wird beendet");

		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (socket != null) {
					socket.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static int ggT(int a, int b) {

		if (b == 0) {
			return a;
		} else {
			return ggT(b, a % b);
		}

	}
}