package main.thread.BasicThreads;

import main.thread.BasicThreads.MyRunnableThread;

public class MainClass {

	public static void main(String[] args) {
		MyRunnableThread m1 = new MyRunnableThread("0");
		MyRunnableThread m2 = new MyRunnableThread("1");
		Thread t1 = new Thread(m1);
		Thread t2 = new Thread(m2);
		t1.start();
		t2.start();
		System.out.println("Threads gestartet");
//		try {
//			t1.join();
//			t2.join();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		System.out.println("Alles fertig");
	}
}