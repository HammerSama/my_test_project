package main.thread.BasicThreads;

public class MyRunnableThread implements Runnable {
//public class MyRunnableThread extends Thread {	
	
	String text;
	public MyRunnableThread(String text) {
		this.text = text;
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(this.text);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}