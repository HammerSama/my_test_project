package main.thread.CallableThread;

import main.thread.CallableThread.MyCallableThread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallMain {

    public static void main(String[] args) {
        int[] numbers = {987654321,713245,6669};

        ExecutorService ex = Executors.newCachedThreadPool();
        Future<Integer>[] solution = new Future[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            solution[i] = ex.submit(new MyCallableThread(numbers[i]));
        }

        for (int i = 0; i < numbers.length; i++) {
            try {
                System.out.println("Checksum of " + numbers[i] + " is " + solution[i].get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }
    }
}
