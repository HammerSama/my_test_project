package main.thread.CallableThread;

import java.util.concurrent.Callable;


public class MyCallableThread implements Callable<Integer> {

    int number;

    public MyCallableThread(int number)
    {
        this.number = number;
    }

    public Integer call() {
        return checksum(this.number);
    }

    private int checksum(int number) {
       if (number <= 9) {
           return number;
       }

       return number%10 + checksum(number/10);
    }
}
