package TCP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Date;
import java.util.Scanner;

public class DatagramServerThreads {

	public static void main(String[] args) {
		// Socket erstellen unter dem der Server erreichbar ist
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(3431);
			while (true) {
				// Neues Paket anlegen
				DatagramPacket packet = new DatagramPacket(new byte[5], 5);
				// Auf Paket warten
				try {
					socket.receive(packet);
					// Empfangendes Paket in einem neuen Thread abarbeiten
					new MyUDPThread(packet, socket).start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} finally {
			socket.close();
		}

	}

}

class MyUDPThread extends Thread {

	private DatagramPacket packet;
	private DatagramSocket socket;

	public MyUDPThread(DatagramPacket packet, DatagramSocket socket)
			throws SocketException {
		this.packet = packet;
		this.socket = socket;
	}

	public void run() {
		// Daten auslesen
		InetAddress address = packet.getAddress();
		int port = packet.getPort();
		int len = packet.getLength();
		byte[] data = packet.getData();

		System.out.println("Anfrage von "+address+" vom Port "+port+" mit der Länge "+len+"\n"+new String(data));

		// Nutzdaten in ein Stringobjekt übergeben
		String da = new String(packet.getData());
		// Kommandos sollen durch : getrennt werden
		try (Scanner sc = new Scanner(da).useDelimiter(":")) {
			// Erstes Kommando filtern
			String keyword = sc.next();

			if (keyword.equals("DATE")) {

				Date dt = new Date();
				byte[] myDate = dt.toString().getBytes();

				// Paket mit neuen Daten (Datum) als Antwort vorbereiten
				packet = new DatagramPacket(myDate, myDate.length, address, port);

				try {
					// Paket versenden
					socket.send(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				byte[] myDate = new byte[1024];
				myDate = new String("Command unknown").getBytes();
				try {
					sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				// Paket mit Information, dass das Schlüsselwort ungültig ist als
				// Antwort vorbereiten
				packet = new DatagramPacket(myDate, myDate.length, address, port);
				try {
					// Paket versenden
					socket.send(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
