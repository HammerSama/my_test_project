package model;

import com.sun.org.apache.xerces.internal.xs.StringList;
import interfaces.Song;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.Playlist;

import java.io.File;
import java.rmi.RemoteException;

/**
 * Created by haoha on 2017/11/1.
 */
public class Model {
    private Playlist alleLieder = new Playlist();
    private Playlist playList = new Playlist();
    private String MusicPath="";
    private File MusicFile ;
    private File[] MusicIndex;
    private String[] MusicName;
    public Model(){
        if(!MusicPath.equals("")){
            MusicFile = new File(MusicPath);
        }else{
            MusicFile = new File("C:\\Users\\haoha\\Music");
        }
        MusicIndex = MusicFile.listFiles();
        MusicName = MusicFile.list();
    }


    public Playlist getAlleLieder() throws RemoteException {
        /*
        for(int i = 0; i<MusicIndex.length;i++){
            Song s = new main.Song();
            s.setTitle(MusicIndex[i].getName());
            s.setPath(MusicIndex[i].getAbsolutePath());
            alleLieder.addSong(s);
        }*/
        return this.alleLieder;
    }

    public Playlist getPlayList(){
        return this.playList;
    }

    public void addNewToAll(Song song)throws RemoteException{
        alleLieder.addSong(song);
    }

    public void addNewToPlaying(Song song) throws RemoteException{
        if(song !=null){
            this.playList.addSong(song);
        }
    }

    public void addAllToPlaying() throws RemoteException {
        this.playList.setList(this.alleLieder.getList());
    }

    public void clearPlaying() throws  RemoteException{
        this.playList.clearPlaylist();
    }

    public void setMusicPath(String s) throws RemoteException {
        if(!s.equals("")){
            this.MusicPath = s;
            MusicFile = new File(s);
            MusicIndex = MusicFile.listFiles();
            MusicName = MusicFile.list();
            for(int i = 0; i<MusicIndex.length;i++){
                Song ss = new main.Song();
                ss.setTitle(MusicIndex[i].getName());
                ss.setPath(MusicIndex[i].getAbsolutePath());
                alleLieder.addSong(ss);
        }}else{
            return;
        }
    }


}
