package view;

import interfaces.Song;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import junit.framework.Test;
import main.Playlist;

import java.io.File;
import java.rmi.RemoteException;

public class View {
    private File MusicFile=new File("D:\\haoha\\Documents");
    private File[] MusicIndex = MusicFile.listFiles();
    //private Playlist playingList;
    Scene playingScene;
    BorderPane playingBP;
    private TextField choiceBox;
    private Button load;
    private Button save;
    private  HBox Top;
    private ListView<Song> allSongs;
    private ListView<Song> playing;
    private HBox bot;
    private Label titleL;
    private TextField titleT;
    private Label interpretL;
    private TextField interpretT;
    private Label albumL;
    private TextField albumT;
    private VBox metaDaten;
    private Button play;
    private Button pause;
    private Button next;
    private Button commit;
    private HBox functionBar;
    private Button addToPlayList;
    private VBox right;
    private Button addAll;
    private Button clearAll;
    private String playPath;
    private Media media;
    MediaPlayer player;
    public View() throws RemoteException {

        playingBP = new BorderPane();
        //topside building
        choiceBox = new TextField();
        choiceBox.setMinSize(200,0);
        load = new Button("load");
        save = new Button("save");
        Top = new HBox(choiceBox,load,save);
        playingBP.setTop(Top);
        ///bottom side building
        //list building
        allSongs = new ListView<interfaces.Song>();
        allSongs.setMinSize(300,600);

        allSongs.setOnMouseClicked(e->{
            if (allSongs.getSelectionModel().getSelectedItem() != null) {
                setTitleT(allSongs.getSelectionModel().getSelectedItem().titleProperty());
                setInterpretT(allSongs.getSelectionModel().getSelectedItem().interpretProperty());
                setAlbumT(allSongs.getSelectionModel().getSelectedItem().albumProperty());
            }

        });
        playing = new ListView<interfaces.Song>();
        playing.setMinSize(300,600);
        playing.setOnMouseClicked(e->{
            if(playing.getSelectionModel().getSelectedItem()!=null){
                setTitleT(playing.getSelectionModel().getSelectedItem().titleProperty());
                setInterpretT(playing.getSelectionModel().getSelectedItem().interpretProperty());
                setAlbumT(playing.getSelectionModel().getSelectedItem().albumProperty());
            }

        });
        //meta date building
        titleL = new Label("Title:");
        titleT = new TextField();
        SimpleStringProperty Titel = new SimpleStringProperty();
        Titel.setValue("abc");
        titleT.setMinSize(250,0);
        interpretL = new Label("Interpret");
        interpretT = new TextField();
        interpretT.setMinSize(250,0);
        albumL = new Label("Album:");
        albumT = new TextField();
        albumT.setMinSize(250,0);
        metaDaten = new VBox(titleL, titleT, interpretL, interpretT, albumL, albumT);
        //function bar building
        /*
        //playPath = getClass().getResource("D:\\haoha\\Documents\\FPT2\\FXStringListMultiViews\\abc.mp3").toString();
        playPath = new File("D:\\haoha\\Documents\\FPT2\\FXStringListMultiViews\\abc.mp3").getAbsolutePath();
        media = new Media(playPath);
        player = new MediaPlayer(media);
        */
        play = new Button("pl");
        pause = new Button("pa");
        next = new Button("ne");
        commit = new Button("commit");
        functionBar = new HBox(play,pause,next,commit);
        addAll = new Button("Add All");
        addToPlayList = new Button("Add to Play List");
        clearAll = new Button("clear all");
        right = new VBox(metaDaten,functionBar,addToPlayList,addAll,clearAll);
        //make together
        bot = new HBox(allSongs, playing,right);
        playingBP.setBottom(bot);
        playingScene = new Scene(playingBP);
    }
    public Scene getPlayingScene(){
        return this.playingScene;
    }
    public File[] getMusicIndex(){
        return MusicIndex;
    }
    public ListView<Song> getAllSongs() {
        return allSongs;
    }
    public ListView<Song> getPlaying() {
        return playing;
    }
    public void setTitleT(ObservableValue<String> s){
        this.titleT.textProperty().bind(s);
        //this.titleT.setText(s.getValue());
    }
    public void addEventAddAll(EventHandler<ActionEvent> eventHandler){
        addAll.addEventHandler(ActionEvent.ACTION, eventHandler);
    }
    public void addEventAddToPlayList(EventHandler<ActionEvent> eventEventHandler){
        addToPlayList.addEventHandler(ActionEvent.ACTION,eventEventHandler);
    }

    public void addEventCommit(EventHandler<ActionEvent> eventEventHandler){
        commit.addEventHandler(ActionEvent.ACTION,eventEventHandler);
    }
    public void addEventClearAll(EventHandler<ActionEvent> eventEventHandler){
        clearAll.addEventHandler(ActionEvent.ACTION,eventEventHandler);
    }
    public void addEventLoad(EventHandler<ActionEvent> eventEventHandler){
        load.addEventHandler(ActionEvent.ACTION,eventEventHandler);
    }

    public void setInterpretT(ObservableValue<String>s){
        this.interpretT.setText(s.getValue());
    }
    public String getInterpretT(){
        return this. interpretT.getText();
    }
    public void setAlbumT(ObservableValue<String> s){
        this.albumT.setText(s.getValue());
    }
    public String getAlbumT(){
        return this.albumT.getText();
    }

    public void setListViewShow(ListView l){

        l.setCellFactory(c -> {

            ListCell<Song> cell = new ListCell<Song>() {
                @Override
                protected void updateItem(Song myObject, boolean b) {
                    super.updateItem(myObject, myObject == null || b);
                    if (myObject != null) {
                        setText( myObject.getTitle());
                    } else {
                        setText("");
                    }
                }

            };
            cell.setContentDisplay(ContentDisplay.LEFT);
            return cell;

        });

    }

    public Song getSelected(){
        return allSongs.getSelectionModel().getSelectedItem();
    }
    public String getPath(){
        return this.choiceBox.getText();
    }



}
